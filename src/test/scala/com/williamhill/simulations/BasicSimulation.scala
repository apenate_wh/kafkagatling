package com.williamhill.simulations

import com.github.mnogu.gatling.kafka.Predef._
import io.gatling.core.Predef._
import org.apache.kafka.clients.producer.ProducerConfig

import scala.concurrent.duration._

class BasicSimulation extends Simulation{
  val kafkaConf = kafka
    .topic("foo")
    .properties(
      Map(
        ProducerConfig.ACKS_CONFIG -> "1",
        // list of Kafka broker hostname and port pairs
//        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG -> "b-1.pds-pp1.mbxcdy.c3.kafka.eu-west-1.amazonaws.com:9094,b-2.pds-pp1.mbxcdy.c3.kafka.eu-west-1.amazonaws.com:9094,b-3.pds-pp1.mbxcdy.c3.kafka.eu-west-1.amazonaws.com:9094",
        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG -> "b-1.pds-dev.etmz8v.c3.kafka.eu-west-1.amazonaws.com:9094,b-2.pds-dev.etmz8v.c3.kafka.eu-west-1.amazonaws.com:9094",
        "security.protocol" -> "SASL_SSL",
        "sasl.mechanism" -> "SCRAM-SHA-512",
        "sasl.jaas.config" -> "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"test\" password=\"test\";",
        // in most cases, StringSerializer or ByteArraySerializer
        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG ->
          "org.apache.kafka.common.serialization.StringSerializer",
        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG ->
          "org.apache.kafka.common.serialization.StringSerializer"))

  val scn = scenario("Kafka Test")
    .exec(
      kafka("request")
        // message to send
        .send[String]("bar"))

  setUp(
    scn
      .inject(atOnceUsers(1)))
    .protocols(kafkaConf)
}
